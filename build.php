<?php

/**
 * @file
 *   Build a Drupal 6 ready package out of files in this folder.
 */

// Clean up build directory.
`rm -rf ./build`;

$files = glob('*.po');
foreach ($files as $file) {
  if (!strpos($file, '-')) {
    if ($file == 'installer.po') {
      // Special file, goes to install profile.
      $target = 'profiles/default/translations/lt.po';
    }
    else {
      // 'Root' files go to system module.
      $target = 'modules/system/translations/'. str_replace('.po', '.lt.po', $file);
    }
  }
  else {
    // Other files go to their module or theme folder.
    $target = str_replace(array('-', '.po'), array('/', ''), $file) .'/translations/'. str_replace('.po', '.lt.po', $file);
  }
  $target = 'build/'. $target;
  
  // Create target folder and copy file there.
  mkdir(dirname($target), 0777, TRUE);
  copy($file, $target);
}

// Build package.
unlink('lt-6.0-dev.tar.gz');
`cd build && tar -cvzf ../lt-6.0-dev.tar.gz * && cd ..`;

// Clean up build directory.
//`rm -rf ./build`;
